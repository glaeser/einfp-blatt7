#ifndef STACK
#define STACK

struct node2 {
	int data;
	struct node2 * next;
};
typedef struct node2 node;

node * new_node(int data);
void free_node(node * node);
int is_empty(node* head);
int peek(node* head);
void push(node** head, int data);
int pop(node** head);
#endif /* SET */
