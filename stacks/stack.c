#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include "stack.h"


// struct node2 {
// 	int data;
// 	struct node2 * next;
// };
// typedef struct node2 node;

//TODO

// Initialize a node with value data and returns a reference to it
node * new_node(int data) {
	node * e = (node *)malloc(sizeof(node));
	e->data =data;
	e->next = NULL;
	return e;
}

// Free Memory allocated to a node
void free_node(node * node){
	free(node);
}

//returns 1 if list represented by head is empty, 0 otherwise
int is_empty(node* head){
	//TODO
}

//returns value of first element of list represented by head
//May return INT_MIN, if list is empty, but must not throw an error
int peek(node* head){
	//TODO
}

// Inserts a new node with value data in the beginning of the list represented by head
void push(node** head, int data){
	//TODO
}

//deletes the first element of the list represented by hard head and returns it value,
//May return INT_MIN, if list is empty, but must not throw an error
int pop(node** head){
	//TODO
}
