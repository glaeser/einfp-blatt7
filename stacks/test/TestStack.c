#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include "stack.h"
#include "unity.h"


void setUp(void)
{
}

void tearDown(void)
{
}

void test_stack(){
	node * head = NULL;
	for (int i = 1; i < 5; i++) {
		push(&head,i);
		TEST_ASSERT_EQUAL_INT(i,peek(head));
	}
	for (int i = 1; i < 5; i++) {
		TEST_ASSERT_EQUAL_INT(5-i,pop(&head));
	}
	TEST_ASSERT_EQUAL_INT(INT_MIN,peek(head));
	TEST_ASSERT_EQUAL_INT(INT_MIN,pop(&head));

	for (int i = 1; i < 3; i++) {
		push(&head,i);
		TEST_ASSERT_EQUAL_INT(i,peek(head));
	}
	for (int i = 1; i < 3; i++) {
		TEST_ASSERT_EQUAL_INT(3-i,pop(&head));
	}
}
