#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "stack.h"

int main(void) {
	node * head = NULL;
	for (int i = 1; i < 5; i++) {
		push(&head,i);
    printf("Peeked at %d\n", peek(head));
	}
	for (int i = 1; i < 6; i++) {
		printf("Popped %d\n", pop(&head));
	}

	for (int i = 1; i < 3; i++) {
    printf("Peeked at %d\n", peek(head));
		push(&head,i);
	}
	printf("Peeked at %d\n", peek(head));
	for (int i = 1; i < 3; i++) {
		printf("Popped %d\n", pop(&head));
	}
}
